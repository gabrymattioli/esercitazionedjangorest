from config import base

import requests
import unittest


class AuthenticationTest(unittest.TestCase):
    def test_registration_and_login(self):
        # Talking with her friends, Debora gets to know a new Website that allows users to save their favourite films.
        # Going to check out its homepage, she notices it is necessary to create a new account.
        registration_data = {
            "username": "debora98_",
            "password1": "mypass123",
            "password2": "mypass123"
        }
        post_response = requests.post(
            f"{base.api_url}/dj-rest-auth/registration/", json=registration_data
        )

        # She hits enter and the account has been created.
        self.assertEqual(post_response.status_code, 201)

        # She wants to login, so she fills the form with her data and hits Enter.
        self.login_data = {
            "username": "debora98_",
            "password": "mypass123"
        }
        post_response = requests.post(f"{base.api_url}/dj-rest-auth/login/", json=self.login_data)
        self.assertEqual(post_response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
